package com.company;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.ExportException;

public class Server  {
    ServerSocket server = null;
    Socket client = null;
    String stringaRicevuta= null;
    String stringaModificata=null;
    BufferedReader inDalClient;
    DataOutputStream outVersoClient;

    public Socket attendi(){
        try{
            System.out.println("Server partito in esecuzione");

            server = new ServerSocket(1111);

            client= server.accept();

            server.close();

            inDalClient=  new BufferedReader(new InputStreamReader(client.getInputStream()));
            outVersoClient= new DataOutputStream(client.getOutputStream());
        }catch (ExportException e){
            System.out.println(e.getMessage());
            System.out.println("errore durante l istanza del server");
            System.exit(1);
        } catch (IOException e) {
                e.printStackTrace();
        }
        return client;
    }

    public void comunica(){
        try{
            System.out.println("Benvenuto client,scrivi una frase e la trasformo in maiuscolo, attendo ...");
            stringaRicevuta= inDalClient.readLine();
            System.out.println("ricevuta la string dal client"+stringaRicevuta);

            stringaModificata=stringaRicevuta.toUpperCase();
            System.out.println("invio la stringa modificata al client");
            outVersoClient.writeBytes(stringaModificata);
            System.out.println("Fine elaborazione , buonanotte");
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]){
        Server  server= new Server();
        server.attendi();
        server.comunica();
    }

}
