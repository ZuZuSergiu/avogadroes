package com.company;

import jdk.swing.interop.SwingInterOpUtils;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final double DISTANZA = 0.0;


        Scanner reader = new Scanner(System.in);
         /*a) permetta di inserire il numero di partecipanti
   b) permetta di inserire per ciascun partecipante nome, velocita
   c) permetta di inserire per tutti i partecipanti la stessa distanza da coprire
   d) per ciascun partecipante visualizzi in console solo quando inizia e solo quando termina la corsa
   e) alla fine della esecuzione si visualizzi l'ordine di arrivo dei partecipanti
   f) introduca una leggera modifica (pari all'1% in più o meno) alla velocità
   ad ogni ciclo di 100 msec (in altri termini la velocità del podista non è più costante ma varia dell'1% in più o in meno ad ogni ciclo)*/

        try {

            System.out.println("Inserisci numero partecipanti");
            int numeroPartecipanti = reader.nextInt();
            Podista[] podisti = new Podista[numeroPartecipanti];
            for (int i = 0; i < podisti.length; i++) {
                System.out.println("Inserisci nome podista");
                reader.nextLine();
                String nome = reader.nextLine();
                System.out.println("Inserisci velocita podista");
                double velocita = reader.nextDouble();
                Podista podista = new Podista(nome, velocita, DISTANZA);
                podista.run();


            }


        } catch (Exception ex) {
            System.out.println("Errore di input");
        }


    }


}

