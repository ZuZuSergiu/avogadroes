package com.company;

import java.util.Scanner;

public class Podista extends Thread {
    final double DISTANZA= 0.0;
    String nome;
    double velocita;
    double distanza;

public Podista(String nome,double velocita,double distanza){
    this.nome=nome;
    this.distanza=distanza;
    this.velocita=velocita / 10;
    }

    public void run() {
        Scanner reader= new Scanner(System.in);

        System.out.println("Inserisci distanza");
        double distanza= reader.nextDouble();
        System.out.println("inizio corsa "+DISTANZA);

        while (this.distanza < distanza) {
            this.distanza+=this.velocita;
            System.out.printf(nome + " ha percorso: %.2f \n", this.distanza );

        }
        System.out.println(nome + " Ha terminato la sua corsa");

        try {
            sleep(100);
        } catch (InterruptedException e) {

        }
}
}
