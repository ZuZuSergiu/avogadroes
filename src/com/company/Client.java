package com.company;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLOutput;

public class Client {
    String nomeServer = "localhost";
    int portaServer = 1111;
    DataInputStream in;
    DataOutputStream out;
    Socket mioSocket;
    BufferedReader tastiera;
    String stringaUtente;
    String stringaRicevutaDalServer;
    DataOutputStream outVersoServer;
    BufferedReader inDalServer;

    public Socket connetti() {
        System.out.println("CLIENT PARTITO IN ESECUZIONE ...");
      try{
          tastiera = new BufferedReader(new InputStreamReader(System.in));
          mioSocket=new Socket(nomeServer,portaServer);

          outVersoServer=new DataOutputStream(mioSocket.getOutputStream());
          inDalServer= new BufferedReader(new InputStreamReader(mioSocket.getInputStream()));
      } catch (UnknownHostException e) {
          System.err.println("HOST SCONOSCIUTO");
      } catch (Exception e) {
          System.out.println(e.getMessage());
          System.out.println("ERRORE DURANTE LA CONNESSIONE");
          System.exit(1);

      }
        return  mioSocket;
    }
    public void comunica(){
        try{
            System.out.println("4...inserisci la stringa da trasmettere al server");
            stringaUtente=tastiera.readLine();
            System.out.println("invio la stringa al server e attendo ...");
            outVersoServer.writeBytes(stringaUtente);
            stringaRicevutaDalServer=inDalServer.readLine();
            System.out.println("Risposta dal server"+stringaRicevutaDalServer);
            System.out.println("CLIENTE: termina elaborazione e chiude connessione");
            mioSocket.close();
        }
        catch (EOFException e){
            System.out.println(e.getMessage());
            System.out.println("ERRORE DURANTE LA CONNESSIONE CON IL SERVER");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String args[]){
        Client client= new Client();
        client.connetti();
        client.comunica();
    }

}
